package com.example.qwertyu.prelimdelacruz111;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText value = (EditText) findViewById(R.id.enter);
        Button btnTry = (Button) findViewById(R.id.btntry);
        Button btnRev= (Button) findViewById(R.id.btnrev);


        final Random rand = new Random();
        final int n = rand.nextInt(10);

        btnTry.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                int num = Integer.valueOf(value.getText().toString());
                if (num == n) {
                    Toast.makeText(MainActivity.this, "Correct Guess!", Toast.LENGTH_SHORT).show();
                }
                if (num < n) {
                    Toast.makeText(MainActivity.this, "Higher!", Toast.LENGTH_SHORT).show();
                }
                if (num > n) {
                    Toast.makeText(MainActivity.this, "Lower!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnRev.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Toast.makeText(MainActivity.this, String.valueOf(n), Toast.LENGTH_LONG).show();
            }
        });

    }}




